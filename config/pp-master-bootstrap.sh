#!/bin/bash
set -e

## Add hosts
cat >> /etc/hosts <<EOF
192.168.56.80 puppet pp-master pp-master.example.com
192.168.56.81 pp-agent
EOF

## Enable Puppet platform on Yum
sudo rpm -Uvh https://yum.puppet.com/puppet7-release-el-7.noarch.rpm

## Install Puppet Server
sudo yum -y update \
  && sudo yum -y install \
    vim \
    ntpd \
    puppetserver

## Start Puppet Server
sudo systemctl enable puppetserver \
  && sudo systemctl start puppetserver \
  && exec bash

## Change Puppet memory allocation 
sudo sed -i 's/-Xms2g -Xmx2g/-Xms512m -Xmx512m/g' /etc/sysconfig/puppetserver

## Set autosign
cat >> /etc/puppetlabs/puppet/puppet.conf <<EOF
autosign = true
EOF

## Restart Puppet
sudo systemctl restart puppetserver


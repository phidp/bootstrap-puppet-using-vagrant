#!/bin/bash
set -e

## Add hosts
cat >> /etc/hosts <<EOF
192.168.56.80 puppet pp-master pp-master.example.com
192.168.56.81 pp-agent
EOF

## Enable Puppet platform on Yum
sudo rpm -Uvh https://yum.puppet.com/puppet7-release-el-7.noarch.rpm

## Install Puppet Server
sudo yum -y update \
  && sudo yum -y install \
    vim \
    ntpd \
    puppet-agent

## Start Puppet Server
sudo systemctl enable puppet \
  && sudo systemctl start puppet

## Restart Puppet
sudo systemctl restart puppet

## Export PATH
export PATH=/opt/puppetlabs/bin:$PATH

## Config setting to connect to the puppet master
sudo puppet config set server pp-master.example.com --section main

## Send certificates to Puppet Master
puppet ssl bootstrap
